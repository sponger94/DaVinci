package com.javohir.davinci.davinci.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "BlogPost")
public class BlogPost {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private Date date;
    private String title;
    private byte[] preview;
    private byte[] content;

    @Ignore
    public BlogPost() {
    }

    @Ignore
    public BlogPost(Date date, String title, byte[] preview, byte[] content) {
        this.date = date;
        this.title = title;
        this.preview = preview;
        this.content = content;
    }

    public BlogPost(int id, Date date, String title, byte[] preview, byte[] content) {
        this.id = id;
        this.date = date;
        this.title = title;
        this.preview = preview;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public byte[] getPreview() {
        return preview;
    }

    public void setPreview(byte[] preview) {
        this.preview = preview;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}
