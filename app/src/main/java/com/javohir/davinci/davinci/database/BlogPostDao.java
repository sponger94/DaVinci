package com.javohir.davinci.davinci.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface BlogPostDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNote(BlogPost postEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<BlogPost> postEntity);

    @Delete
    void deletePost(BlogPost postEntity);

    @Query("SELECT * FROM blogpost WHERE id = :id")
    BlogPost getPostById(int id);

    @Query("SELECT * FROM blogpost ORDER BY date DESC")
    LiveData<List<BlogPost>> getAll();

    @Query("DELETE FROM blogpost")
    int deleteAll();

    @Query("SELECT COUNT(*) FROM blogpost")
    int getCount();
}
