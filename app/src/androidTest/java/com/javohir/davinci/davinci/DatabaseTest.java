package com.javohir.davinci.davinci;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.javohir.davinci.davinci.database.AppDatabase;
import com.javohir.davinci.davinci.database.BlogPost;
import com.javohir.davinci.davinci.database.BlogPostDao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class DatabaseTest {
    private static final String TAG = "Junit";
    private AppDatabase mDb;
    private BlogPostDao mDao;

    @Before
    public void createDb(){
        Context context = InstrumentationRegistry.getTargetContext();
        mDb = Room.inMemoryDatabaseBuilder(context,
                AppDatabase.class).build();
        mDao = mDb.blogPostDao();
        Log.i(TAG, "createDb");
    }

    @After
    public void closeDb(){
        mDb.close();
        Log.i(TAG, "closeDb");
    }

    @Test
    public void createAndRetrievePosts(){
        mDao.insertAll(getSampleData());
        int count = mDao.getCount();
        Log.i(TAG, "createAndRetrieveNotes: count = " + count);
        assertEquals(getSampleData().size(), count);
    }

    @Test
    public void compareString(){
        mDao.insertAll(getSampleData());
        BlogPost original = getSampleData().get(0);
        BlogPost fromDao = mDao.getPostById(1);
        assertEquals(original.getTitle(), fromDao.getTitle());
    }

    private List<BlogPost> getSampleData(){
        List<BlogPost> posts = new ArrayList<>();
        posts.add(new BlogPost());
        posts.add(new BlogPost());
        posts.add(new BlogPost());
        return posts;
    }
}
